// ==UserScript==
// @name         Talibri - Combat Tracker
// @namespace    http://talibri.pirion.net/
// @version      0.3
// @description  Talibri combat tracking tool
// @author       Kaine "Pirion" Adams (0.1) & Eph
// @match        https://talibri.com/*
// @include      https://talibri.com/*
// @grant        none
// ==/UserScript==

var combat = {
    data: null,
    is_stopped: false,
    actionDictionary: { skills: [
        {name: "Stab",successful: true, text: "You lunged at the enemy stabbing them"},
        {name: "Stab",successful: false, text: "You attempted to use Stab"},
        {name: "Shock Strike",successful: true, text: "You channel lightning energy into your blade"},
        {name: "Shock Strike",successful: false, text: "You attempted to use Shock Strike"},
        {name: "Lightning Defense",successful: true, text: "Lightning courses through your body"},
        {name: "Lightning Defense",successful: false, text: "You attempted to use Lightning Defense"},
        {name: "Dash",successful: true, text: "You dash into the enemy's defenses"},
        {name: "Bash",successful: true, text: "You bash the enemy"},
        {name: "Blade Twist",successful: true, text: "Your blade penetrates the enemy"},
        {name: "Blade Twist",successful: false, text: "You attempted to use Blade Twist but failed"},
        {name: "Bash",successful: false, text: "You attempted to use Bash"},
        {name: "ShieldBash",successful: true, text: "You bashed the enemy with your shield"},
        {name: "Shout",successful: true, text: "You shout at the enemy building your adrenaline"},
        {name: "Wound",successful: true, text: "You strike at the enemy in an attempt to wound them"},
        {name: "Wound",successful: false, text: "You attempted to use Wound but failed"},
        {name: "Fiery Strike",successful: true, text: "You cover your weapon in oil and light it ablaze before striking the enemy"},
        {name: "Fiery Strike",successful: false, text: "You attempted to use Fiery Strike"},
        {name: "Aimed Shot",successful: true, text: "You line up the shot and let your arrow fly"},
        {name: "Aimed Shot",successful: false, text: "You attempted to use Aimed Shot"},
        {name: "Rapid Shot",successful: true, text: "One of your arrows launched in quick succession"},
        {name: "Rapid Shot",successful: false, text: "You attempted to use Rapid Shot"},
        {name: "Wing Clip",successful: true, text: "Your shot pinned the enemy"},
        {name: "Wing Clip",successful: false, text: "You attempted to use Wing Clip"},
        {name: "Poison Shot",successful: true, text: "Your poison tipped arrow"},
        {name: "Ignite",successful: true, text: "You ignite your enemy"},
        {name: "Ignite",successful: false, text: "You attempted to use Ignite"},
        {name: "Freeze",successful: true, text: "You freeze your enemy"},
        {name: "Freeze",successful: false, text: "You attempted to use Freeze"},
        {name: "Electrify",successful: true, text: "You Electrify your enemy"},
        {name: "Electrify",successful: false, text: "You attempted to use Electrify"},
        {name: "Earth Eruption",successful: true, text: "The Earth Erupts under your enemy"},
        {name: "Earth Eruption",successful: false, text: "You attempted to use Earth Eruption"},
        {name: "Antipode Blast",successful: true, text: "The fire of your antipode"},
        {name: "Antipode Blast",successful: false, text: "You attempted to use Antipode Blast"},
        {name: "Item Failed",successful: false, text: "You are out of"},
    ]},
    cookiePath: function() {
        return "/";
    },
    cookieName: function() {
        return "/scripts/pirion/combat/data";
    },
    cookieExpirationDays: function() {
        return 7;
    },

    initialize: function() {
        //add jquery.cookie.js:
        $('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>');
        $('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>');
        $('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />');
        $('head').append('<script type="text/javascript">var script = script || {};</script>');

        // stop combat function
        if (!window.stopAdventureCallbacks) {
            window.stopAdventureCallbacks = [stopAdventure];
            stopAdventure = function() {
                for (let i = window.stopAdventureCallbacks.length; i--; i >= 0) {
                    window.stopAdventureCallbacks[i]();
                }
            };
        }
        let stopAdventureCallback = function() {
            combat.data.flee += 1;
            combat.data.rounds += 1;
        };
        if (window.stopAdventureCallbacks.indexOf(stopAdventureCallback) === -1) {
            window.stopAdventureCallbacks.push(stopAdventureCallback);
        }

        window.setTimeout(combat.initalizeUserInterface, 2000);

        combat.start();
    },
    initalizeUserInterface: function() {
        var html = "";
        html += '<div style="width: 80%; margin-left: auto; margin-right: auto;">';
        html += '<div style="width: 80px; margin-left: auto; margin-right: auto;">';
        html += '<a style="width: 80px;" onclick="javascript: script.reset=true;return false;" href="#">Reset Stats</a></div>';
        html += '<table style="width: 80%; margin-left: auto; margin-right: auto;">';
        html += '<tr><th>Actions</th><td id="combat_actions">0</td></tr>';
        html += '<tr><th>Fights (W/L/F)</th><td id="fights">0</td></tr>';
        html += '<tbody><tr><th>Accuracy</th><td id="combat_accuracy">0/0</td></tr>';
        html += '<tr><th>Exp Gained</th><td id="combat_exp">0</td></tr>';
        html += '<tr><th>Leol Gained</th><td id="combat_leol">0</td></tr>';
        html += '<tr><th>Exp/Tick</th><td id="combat_exp_tick">0</td></tr>';
        html += '<tr><th>Leol/Tick</th><td id="combat_leol_tick">0</td></tr>';
        html += '<tr><th>Sec/Tick</th><td id="combat_second_tick">0</td></tr>';
        // html += '<tr><th>Exp/Hour</th><td id="combat_exp_hour">0.00</td></tr>';
        // html += '<tr><th>Leol/Hour</th><td id="combat_leol_hour">0.00</td></tr>';
        combat.actionDictionary.skills.map(skill => skill.name).filter((v, i, a) => a.indexOf(v) === i).map(skill => {
            html += `<tr style="display: none;"><th>${skill} Accuracy</th><td id="combat_${skill.replace(" ", "_")}_accuracy">0/0</td></tr>`;
        });
        html += '</tbody></table></div>';

        if(!combat.data) {
            combat.data = combat.load();
            //if we can't check the cookie,
            //let's skip this round so we don't overwrite it.
            if(!combat.data) {
                return;
            }
        }

        var attachTracker = function() {
            if(!$('#combatTrackerDialog').length && window.location.href.indexOf("combat_zones") > -1) {
                $('body').append('<div id="combatTrackerDialog" title="Combat Tracking">' + html + '</div>');
                $("#combatTrackerDialog").dialog();
            }
            combat.updateUI();
        };
        setInterval(attachTracker, 1000);
    },
    stop: function() {
        combat.is_stopped = true;
    },
    start: function() {
        //add a listener for any ajax page completed
        $(document).ajaxComplete(combat.listen);
        combat.is_stopped = false;
    },
    destory: function() {
        //why would you ever want to do this?
        combat.stop();
        $.removeCookie(combat.cookieName(), {path: combat.cookiePath()});
    },
    reset: function() {
        //overwrite data, and save
        script.reset = false;
        combat.data = combat.new();
        combat.save();

        combat.actionDictionary.skills.map(skill => skill.name).filter((v, i, a) => a.indexOf(v) === i).map(skill => {
            $(`#combat_${skill.replace(" ", "_")}_accuracy`).parent().hide();
        });

        combat.updateUI();
    },
    new: function() {
        //generate an empty json array
        var empty_data = {
            version: 2,
            update: (new Date()),
            since: (new Date()),
            rounds: 0,
            win: 0,
            loss: 0,
            flee: 0,
            inCombat: false,
            leol: 0,
            actions: {},
            items: {},
            monsters: {},
            stats: {},
            affinities: {},
            loot: {}
        };
        return empty_data;
    },
    load: function() {
        //if cookie exists, load, otherwise get a new array.
        var cookie = $.cookie(combat.cookieName());
        if(cookie) {
            var loaded_data = $.parseJSON(cookie);
            return combat.migrate(loaded_data);
        }
        return combat.new();
    },
    migrate: function(migration_data) {
        //if we load, we need to make sure to reset the cookie to a good state.
        if(migration_data.version == 1) {
            //bug caused leol to fail. this will set it to zero.
            migration_data.version = 2;
            migration_data.leol = 0;
        }
        //if we are in combat, lets count as a flee:
        if(migration_data.inCombat == true) {
            migration_data.inCombat = false;
            migration_data.flee += 1;
        }
        return migration_data;
    },
    save: function() {
        //create cookie that expires in 7 days:
        $.cookie(combat.cookieName(), JSON.stringify(combat.data), { path: combat.cookiePath(), expires: combat.cookieExpirationDays() });
    },
    listen: function(event, xhr, settings) {
        //if we've been asked to exit, let's unbind:
        if(combat.is_stopped) {
            $(e.currentTarget).unbind('ajaxComplete');
            return;
        }

        //if ajax response comes from expected location:
        if(settings.url.indexOf('adventure/continue') > -1) {
            if(script && script.reset) {
                combat.reset();
            }
            //if data has not been initialized, initialize it.
            if(!combat.data) {
                combat.data = combat.load();
                //if we can't check the cookie,
                //let's skip this round so we don't overwrite it.
                if(!combat.data) {
                    return;
                }
            }

            if (xhr && xhr.status >= 200 && xhr.status < 300 && xhr.responseText) {
                var result = combat.getResult(xhr.responseText);
                combat.logBattleRound(result);
            }
        }
    },
    getResult: function(response) {
        var result = {
            player: {
                slain: false
            },
            monster: {
                name: "Unknown",
                slain: false
            },
            leol: 0,
            action: {
                type: "Nothing",
                name: "Unknown",
                successful: false
            },
            stats: {},
            affinities: {},
            loot: {}
        };

        if(response.indexOf('You limped back to town on the verge of death') > -1) {
            result.player.slain = true;
        }

        responseLines = response.split(";");

        for(var i = 0; i < responseLines.length; i++) {
            var item = responseLines[i].toLowerCase();
            if(item.indexOf("$('button:contains(\"") > -1) {
                var userItem = item.substr(item.indexOf("$('button:contains(\"")+20);
                result.action.type = "Item";
                result.action.name = userItem.substr(0,userItem.indexOf(" ("));
                result.action.successful = true;

            } else if(item.indexOf("$combat_round.append") > -1 &&
                      item.indexOf("you") > -1)
            {
                var combatText = item.split("\"")[1];

                if(combatText.indexOf("experience.") > -1) {
                    var parts = combatText.split(" ");
                    result.affinities[parts[3]] = parseInt(parts[2]);
                } else if(combatText.indexOf("leol.") > -1) {
                    var parts = combatText.split(" ");
                    result.leol = parseInt(parts[2]);
                } else if(combatText.indexOf("experience,") > -1) {
                    var parts = combatText
                        .replace("you gained ","")
                        .split(", ");
                    for(var j = 0; j < parts.length; j++){
                        var experienceString = parts[j].split(" ");
                        result.stats[experienceString[1]] = parseInt(experienceString[0]);
                    }
                    result.leol = parseInt(parts[2]);
                } else if(combatText.indexOf("you killed the ") > -1) {
                    result.monster.name = combatText.replace("you killed the ","").replace("! <br/>","");
                    result.monster.slain = true;
                } else if(combatText.indexOf(". you now have ") > -1) {
                    var itemStringParts = combatText.substr(11, combatText.indexOf("(")-11).split(" ");
                    var itemString = "";
                    for(var k = 1; k < itemStringParts.length; k++){
                        itemString += (itemString == "" ? "" : " ") + itemStringParts[k];
                    }
                    result.loot[itemString] = parseInt(itemStringParts[0]);
                } else {
                    if(result.action.name == "Unknown") {
                        for(var l = 0; l < combat.actionDictionary.skills.length; l++) {
                            if(combatText.startsWith(combat.actionDictionary.skills[l].text.toLowerCase())) {
                                result.action = combat.actionDictionary.skills[l];
                                result.action.type = "Skill";
                            }
                        }
                    }
                }

            }
        }

        return result;
    },
    logBattleRound: function(result){
        //update the data array:
        combat.data.update = new Date();
        combat.data.rounds += 1;
        combat.data.leol += result.leol;

        if(result.player.slain) {
            combat.data.loss += 1;
        }

        //create action stub if not exists:
        if(result.action.type=="Skill") {
            if(!combat.data.actions[result.action.name]) {
            combat.data.actions[result.action.name] = {successful: 0, unsuccessful: 0};
        }

            //count action:
            if(result.action.successful) {
                combat.data.actions[result.action.name].successful += 1;
            } else {
                combat.data.actions[result.action.name].unsuccessful += 1;
            }
        } else if(result.action.type=="Item") {
            if(!combat.data.items[result.action.name]) {
                combat.data.items[result.action.name] = {count: 1};
            } else {
                combat.data.items[result.action.name].count += 1;
            }
        }
        //if monster was slain, count:
        if(result.monster.slain) {
            if(!combat.data.monsters[result.monster.name]) {
                combat.data.monsters[result.monster.name] = 1;
            } else {
                combat.data.monsters[result.monster.name] += 1;
            }
            combat.data.inCombat = false;
            combat.data.win += 1;
        } else {
            combat.data.inCombat = true;
        }

        for(var stat in result.stats)
        {
            if(!combat.data.stats[stat]) {
                combat.data.stats[stat] = result.stats[stat];
            } else {
                combat.data.stats[stat] += result.stats[stat];
            }
        }

        for(var affinity in result.affinities)
        {
            if(!combat.data.affinities[affinity]) {
                combat.data.affinities[affinity] = result.affinities[affinity];
            } else {
                combat.data.affinities[affinity] += result.affinities[affinity];
            }
        }

        for(var item in result.loot)
        {
            if(!combat.data.loot[item]) {
                combat.data.loot[item] = result.loot[item];
            } else {
                combat.data.loot[item] += result.loot[item];
            }
        }

        //make changes to the cookie
        combat.save();
        //make an update to the visible UI
        combat.updateUI();
    },
    updateUI: function() {
        var ui = $('#combatTrackerDialog')[0];
        if(!ui) {
            return;
        }
        var runtime = (new Date(combat.data.update) - new Date(combat.data.since))/(3600000);
        var successRate = (combat.data.win) / ((combat.data.win + combat.data.loss + combat.data.flee) || 1);
        $("#fights")[0].innerText = `${combat.data.win} | ${combat.data.loss} | ${combat.data.flee} - ${Math.round(successRate * 100)}%`;
        var success = 0;
        var failure = 0;
        for(var action in combat.data.actions) {
            var cur_success = combat.data.actions[action].successful;
            var cur_failure = combat.data.actions[action].unsuccessful;
            success += cur_success;
            failure += cur_failure;
            if (cur_success+cur_failure) {
                $(`#combat_${action.replace(" ", "_")}_accuracy`)[0].innerText = cur_success.toString() + ' of ' + (cur_success+cur_failure).toString() + ' ('+ (Math.round(cur_success/(cur_success+cur_failure)*10000)/100).toString() + '%)';
                $(`#combat_${action.replace(" ", "_")}_accuracy`).parent().show();
            } else {
                $(`#combat_${action.replace(" ", "_")}_accuracy`).parent().hide();
            }
        }
        $("#combat_accuracy")[0].innerText = success.toString() + ' of ' + (success+failure).toString() + ' ('+ (Math.round(success/(success+failure)*10000)/100).toString() + '%)';
        $("#combat_actions")[0].innerText = combat.data.rounds.toString();
        var exp_all = 0;
        for(var stat in combat.data.stats) {
            exp_all += combat.data.stats[stat];
        }
        $("#combat_exp")[0].innerText = exp_all.toString();
        $("#combat_leol")[0].innerText = combat.data.leol.toString();
        $("#combat_exp_tick")[0].innerText = (Math.round(exp_all / combat.data.rounds)).toString();
        $("#combat_leol_tick")[0].innerText = (Math.round(combat.data.leol / combat.data.rounds)).toString();
        $("#combat_second_tick")[0].innerText = ((new Date() - new Date(combat.data.since)) / (1000 * combat.data.rounds)).toFixed(3);
    },
    getString: function() {
        return JSON.stringify(combat.data);
    },
    log: function() {
        console.log(combat.getString());
    }
};

//start the script:
combat.initialize();
